/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.calculadoraderendimento;

/**
 *
 * @author Administrador
 */
public class Carro {

   

    private double KmInicial;

    private double KmFinal;
    
    private double litros;
    
     private double Valor;

    public Carro() {
    }

    public Carro(double KmInicial, double KmFinal, double litros, double Valor) {
        this.KmInicial = KmInicial;
        this.KmFinal = KmFinal;
        this.litros = litros;
        this.Valor = Valor;
    }

    public double getKmInicial() {
        return KmInicial;
    }

    public void setKmInicial(double KmInicial) {
        this.KmInicial = KmInicial;
    }

    public double getKmFinal() {
        return KmFinal;
    }

    public void setKmFinal(double KmFinal) {
        this.KmFinal = KmFinal;
    }

    public double getLitros() {
        return litros;
    }

    public void setLitros(double litros) {
        this.litros = litros;
    }

    public double getValor() {
        return Valor;
    }

    public void setValor(double Valor) {
        this.Valor = Valor;
    }
    
     
     public double getConsumo() {
         return (this.KmInicial - this.KmFinal )/ this.litros ;
        
    }

     
      public double getLucro() {
        return this.Valor - (this.litros* 1.9 );
    }

   
}
