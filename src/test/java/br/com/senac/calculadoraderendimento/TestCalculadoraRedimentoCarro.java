/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.calculadoraderendimento;

import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Administrador
 */
public class TestCalculadoraRedimentoCarro {

    @Test
    
  
    public void deveConsumir1ParaCorridaDe10Km() {
        
        Carro carro = new Carro(0, 10, 10, 100);
        
        double mediaConsumo = carro.getConsumo();
        
        assertEquals(1, mediaConsumo, 0);
    }

    @Test
    public void deveObteLucroDe81ParaCorridaDe10Km() {
        
        Carro carro = new Carro(0, 10, 10, 100);
        
        double lucro = carro.getLucro();
        
        assertEquals(81, lucro, 0);
    }
}
